//
//  TaskModel.swift
//  TaskIt
//
//  Created by Neil on 12/11/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import Foundation

struct TaskModel {
    var task: String
    var subtask: String
    var date: String
}