//
//  ViewController.swift
//  TaskIt
//
//  Created by Neil on 12/11/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.

// Here's today's latest new comment (28 Jan 11:31)
// Testing new git version

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var taskArray:[TaskModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let task1 = TaskModel(task: "Study French", subtask: "Verbs", date: "05/11/2014")
        let task2 = TaskModel(task: "Eat Dinner", subtask: "Burgers", date: "07/11/2014")
        let task3 = TaskModel(task: "Gym", subtask: "Leg day", date: "15/11/2014")

        taskArray = [task1, task2, task3]

        self.tableView.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        <#code#>
    }
    
    // UITableViewDataSource (Required to conform to UITableViewDataSource protocol)

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        var cell: TaskCell = tableView.dequeueReusableCellWithIdentifier("myCell") as TaskCell

        println("Section: \(indexPath.section) Row: \(indexPath.row)")
        
        cell.taskLabel.text = taskArray[indexPath.row].task
        cell.descriptionLabel.text = taskArray[indexPath.row].subtask
        cell.dateLabel.text = taskArray[indexPath.row].date

        return cell
    }

    
    // UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        println(indexPath.row)
        performSegueWithIdentifier("showTaskDetail", sender: self)
    }
    
    
}
